export interface Field {
    type: string;
    name: string;
    label: string;
    required: boolean;
    options?: Options[];
    min?: number;
    max?: number;
}

export interface Options {
    value: any;
}
