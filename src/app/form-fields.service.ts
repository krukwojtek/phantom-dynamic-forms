import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Field} from './models/fields';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class FormFieldsService {

  constructor(private http: HttpClient) { }

  getFields(uuid): Observable<Field[]> {
    return this.http.get<Field[]>(`${environment.apiUrl}/api/phantom_form/` + uuid);
  }

  sendForm(body, uuid): any {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    // tslint:disable-next-line:object-literal-shorthand
    return this.http.post(`${environment.apiUrl}/api/phantom_form/` + uuid, body, {headers: headers}).subscribe(
      data => {
        return this.http.get(`${environment.apiUrl}/api/phantom_form/` + uuid + '/result').subscribe(
          data2 => {
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }
}
