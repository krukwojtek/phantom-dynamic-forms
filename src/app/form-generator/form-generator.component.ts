import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { merge } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { FormFieldsService } from '../form-fields.service';
import { Field } from '../models/fields';

@Component({
  selector: 'app-form-generator',
  templateUrl: './form-generator.component.html',
  styleUrls: ['./form-generator.component.css']
})
export class FormGeneratorComponent implements OnInit {

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private service: FormFieldsService,
    private http: HttpClient,
    private snackBarInvalid: MatSnackBar
  ) { }


  fields: Field[] = [];
  show = false;
  form: FormGroup;
  uuid = '';
  spinner = false;
  textCount = 0;

  subbmited = false;


  ngOnInit(): void {
    this.service = new FormFieldsService(this.http);

    this.route.paramMap.subscribe(params => {
      this.uuid = params.get('uuid');
      if (this.uuid !== '' && this.uuid != null){
        merge()
        .pipe(
          startWith({}),
          switchMap(() => {
            return this.service.getFields(this.uuid);
          }),
          map(data => {
            return data;
          }))
          .subscribe(
            data => {
              this.fields = this.sortField(data);
              this.generateForm();
              this.show = true;
              }
        );
      }
    });
  }


  sortField(data: Field[]): any {
    let tmp = 0;

    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < data.length; index++) {
      if (data[index].type === 'text') {
        this.fields[tmp] = data[index];
        this.textCount++;
        tmp++;
      }
    }
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < data.length; index++) {
      if (data[index].type === 'dropdown') {
        this.fields[tmp] = data[index];
        this.textCount++;
        tmp++;
      }
    }
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < data.length; index++) {
      if (data[index].type === 'textarea') {
        this.fields[tmp] = data[index];
        tmp++;
      }
    }
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < data.length; index++) {
      if (data[index].type === 'radio') {
        this.fields[tmp] = data[index];
        tmp++;
      }
    }
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < data.length; index++) {
      if (data[index].type === 'checkbox') {
        this.fields[tmp] = data[index];
        tmp++;
      }
    }
    return this.fields;
  }

  generateForm(): void {
    const group = {};
    for (const f of this.fields) {
      if (f.type === 'checkbox' ){
        const opts = {};
        for (const opt of f.options) {
          opts[opt.value] = new FormControl(false);
        }
        group[f.name] = new FormGroup(opts);
      }
      else{
        if (f.required === true) {
          group[f.name] = new FormControl('', [Validators.required]);
        }
        else {
          group[f.name] = new FormControl('');
        }
      }
    }
    this.form = new FormGroup(group);
  }


  openSnackBarInvalid(): void {
    this.snackBarInvalid.open('Proszę uzupełnić wymagane pola!', '', {
      duration: 2000,
      panelClass: ['snackbar-invalid-form']
    });
  }

  openSnackBarValid(): void {
    this.snackBarInvalid.open('Formularz wysłany!', '', {
      duration: 2000,
      panelClass: ['snackbar-valid-form']
    });
  }

  changeBooleanOnFalse(key: string): void {
    this.subbmited = false;
    document.getElementById(key).style.color = 'red';
  }

  changeBooleanOnTrue(key: string): void {
    this.subbmited = false;
    document.getElementById(key).style.color = '#191c63';
  }

  onSubmit(): void {

    let send = true;
    let index = 0;

    Object.keys(this.form.controls).forEach(key => {
      if (this.form.controls[key].value === '' && this.fields[index].required === true
       && this.fields[index].type !== 'radio' && this.fields[index].type !== 'checkbox'){
        send = false;
      }
      if (this.form.controls[key].value !== '' && this.fields[index].required === true && this.fields[index].type === 'radio'){
        this.changeBooleanOnTrue(key);
      }
      if (this.form.controls[key].value === '' && this.fields[index].required === true && this.fields[index].type === 'radio'){
        this.changeBooleanOnFalse(key);
        send = false;
      }
      if (this.fields[index].type === 'checkbox' && this.fields[index].required === true){
        let trueCount = 0;
        for (const f of this.fields[index].options) {
          if (this.form.controls[key].get(f.value).value) {
            trueCount++;
          }
        }

        if (this.fields[index].min <= trueCount && trueCount <= this.fields[index].max) {
          this.changeBooleanOnTrue(key);
        }
        else {
          send = false;
          this.changeBooleanOnFalse(key);
        }
      }
      index++;
    });

    if (send === true ){
      this.spinner = true;
      const body = JSON.stringify(this.form.value);
      this.service.sendForm(body, this.uuid);
      this.openSnackBarValid();

    }
    else{
      this.openSnackBarInvalid();

    }
  }
}
