import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FormGeneratorComponent } from './form-generator/form-generator.component';

const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'phantom_form/:uuid', component: FormGeneratorComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
