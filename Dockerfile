# stage 1
FROM node:latest as node 
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/Phantom /usr/share/nginx/html

# FROM node:latest AS builder
# COPY . ./phantom-dynamic-forms
# WORKDIR /phantom-dynamic-forms
# RUN npm i
# RUN $(npm bin)/ng build --prod

# FROM nginx:1.15.8-alpine
# COPY --from=builder /phantom-dynamic-forms/dist/phantom-dynamic-forms/ /usr/share/nginx/html